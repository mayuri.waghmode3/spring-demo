*  Spring uses Factory pattern for handling and creating onjects.

*  Triangle - Bean
*  Application Context, BeanFactory used to instantiate rather than traditional new usage.
*  which uses getBean to instantiate object and assign to calling class object.
*  Blueprint - spring.xml i.e configuration file for all beans in the application
*  beacnFactory refers spring.xml for details of beans for object creation 
*  A bean contains property tags for setter injection or
*  constructor-arg tag for constructor injection both used to predefine a value for attributes of the bean 

