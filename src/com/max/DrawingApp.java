package com.max;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class DrawingApp {
    public static void main(String args[]){
//        Triangle triangle = new Triangle();
//        triangle.draw();
        //beanfactory interface used to instantiate obj rather than new
        //spring.xml in root path
//        BeanFactory factory = new XmlBeanFactory(new FileSystemResource("spring.xml"));
//        Triangle triangle1 = (Triangle) factory.getBean("triangle");
//        triangle1.draw();


        //move spring.xml to class path
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        context.registerShutdownHook();
        Triangle triangle1 = (Triangle) context.getBean("triangle-alias");
        Triangle triangle2 = (Triangle) context.getBean("triangle-alias");
        triangle2.draw();

        //initializing collections explained
        Square square = (Square) context.getBean("square");
        square.draw();

        //Bean Autowiring
        Rombus rombus1 = (Rombus) context.getBean("rombus");
        Rombus rombus2 = (Rombus) context.getBean("rombus");
        rombus1.draw();
        //scope - prototype
        System.out.println(triangle1);
        System.out.println(triangle2);
        //scope(default) - singleton
        System.out.println(rombus1);
        System.out.println(rombus2);

        //using interface
        Shape shape = (Shape)context.getBean("circle");
        shape.draw();
        System.out.println(context.getMessage("greet",null,"default message",null));
    }
}
