package com.max;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;

public class Circle implements Shape, ApplicationEventPublisherAware {
    private Point center;
    @Autowired
    private MessageSource messageSource;
    private ApplicationEventPublisher publisher;
    @Override
    public void draw() {
        System.out.println(messageSource.getMessage("draw.circle",null,"Default draw message",null));
        System.out.println(messageSource.getMessage("center.points",new Object[] {center.getX(),center.getY()},"Default points",null));
        DrawEvent event = new DrawEvent(this);
        publisher.publishEvent(event);
    }

    public Point getCenter() {
        return center;
    }

    @Required
    @Autowired
    @Qualifier("circleRelated")
    public void setCenter(Point center) {
        this.center = center;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher; //Application context
    }
}
