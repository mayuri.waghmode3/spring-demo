package com.max;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Rombus implements ApplicationContextAware, BeanNameAware, InitializingBean, DisposableBean {
    private Point pointA;
    private Point pointB;
    private Point pointC;
    private Point pointD;
    private ApplicationContext context;
    public void draw(){
        System.out.println("Draw Rhombus :\n"+getPointA().getX()+","+getPointA().getY() );
        System.out.println(getPointB().getX()+","+getPointB().getY() );
        System.out.println(getPointC().getX()+","+getPointC().getY() );
        System.out.println(getPointD().getX()+","+getPointD().getY() );
    }
    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public Point getPointD() {
        return pointD;
    }

    public void setPointD(Point pointD) {
        this.pointD = pointD;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("Bean name : "+name);
    }
    public void init(){
        System.out.println("init method called from rombus after initializationtriangle bean");
    }

    public void cleanUp(){
        System.out.println("cleanUp method called from rombus before destroying triangle bean");
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean afterPropertiesSet method called from rombus after initialization triangle bean");
    }

    //wont execute as scope is prototype
    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean destroy method called from rombus before destroying triangle bean");
    }
}
