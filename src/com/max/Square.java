package com.max;

import java.util.List;

public class Square {
    List<Point> points;

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
    public void draw(){
        System.out.println("Draw Squate :");
        for(Point point : points)
            System.out.println(point.getX()+","+point.getY());

    }

}
