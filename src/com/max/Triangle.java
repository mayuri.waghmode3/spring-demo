package com.max;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

public class Triangle implements Shape {

    private Point pointA;
    private Point pointB;
    @Resource(name="zeroPoint")
    private Point pointC;
    private String name;


    public void draw(){
        System.out.println("Draw Triangle :\n"+getName()+getPointA().getX()+","+getPointA().getY() );
        System.out.println(getPointB().getX()+","+getPointB().getY() );
        System.out.println(getPointC().getX()+","+getPointC().getY() );
    }

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PostConstruct
    public void init(){
        System.out.println("init from triangle ---postConstruct JSR-250");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("destroy from triangle ---preDestroy JSR-250");
    }


}
