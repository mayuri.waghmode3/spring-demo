package com.max;

import org.springframework.beans.factory.config.BeanPostProcessor;

public class DisplayNameBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object arg0,String arg1){
        System.out.println("Before initialization. Bean Name :"+arg1);
        return arg0;
    }
    @Override
    public Object postProcessAfterInitialization(Object arg0,String arg1){
        System.out.println("After initialization. Bean Name :"+arg1);
        return arg0;
    }

}
